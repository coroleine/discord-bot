const command = require('./command')

module.exports = class Play extends command {

    static match(message)
    {
        return message.content.startsWith('!play')
    }

    static action(message)
    {
        const YoutubeStream = require('youtube-audio-stream')
        let voiceChannel = message.member.voiceChannel
        console.log(voiceChannel)
        let args = message.content.split(' ')

        voiceChannel
            .join()
            .then(function (connection) {
                let stream = YoutubeStream(args[1])
                connection.playStream(stream).on('end', function () {
                    connection.disconnect()
                })
            })
    }


}