const command = require('./command')

module.exports = class Help extends command {

    static match(message)
    {
        return message.content.startsWith('!help')
    }

    static action(message)
    {
        message.channel.send(' ```Liste des commandes:' +
            '\n- !de (args): par defaut donne un chiffre entre 0 et 10 compris et si avec un argument entre 0 et args.' +
            '\n- !play (lien): joue la musique a l\'adresse qui est le lien.' +
            '\n- !leave: fait quitter le bot du salon vocal. ```')
    }
}