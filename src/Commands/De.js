const command = require('./command')

module.exports = class De extends command {

    static match(message) {
        return message.content.startsWith('!de')
    }

    static action(message) {
        if(message.channel.name === "🎲lancement-des-dés") {
            let args = message.content.split(' ')

            if (args[1] > 0) {
                message.reply(Math.floor(Math.random() * Math.floor(args[1])) + 1)

            }
            else {
                message.reply(Math.floor(Math.random() * Math.floor(100)) + 1)
            }
        }
        else{
            message.reply("Vous ne pouvez pas lancer le dé dans ce lieu")
        }
    }
}