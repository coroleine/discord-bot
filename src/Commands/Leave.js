const command = require('./command')

module.exports = class Leave extends command {

    static match(message)
    {
        return message.content.startsWith('!leave')
    }

    static action(message) {
        let voiceChannel = message.member.voiceChannel

        voiceChannel.leave()
    }
}