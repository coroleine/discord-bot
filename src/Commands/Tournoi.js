const command = require('./command')

const id = 1;

function edit(message) {


    let args = message.content.split(' ');
    var elem = Document.getElementById("win"+id);

    if(args[3]==="win"){
        elem.textContent= args[1];
    }
    else{
        elem.textContent= args[2];
    }
    id +=1;
}

module.exports = class Tournoi extends command {

    static match(message) {
        return message.content.startsWith('!tournoi');
    }

    static action(message) {
        if(message.channel.name === "tournoi") {
            let args = message.content.split(' ');


            message.channel.send(args[1] + " a " + args[3] + " le round contre " + args[2]);
            edit(message)

        }
        else{
            message.reply("Vous ne pouvez pas éditer le tournoi ici");
        }
    }
}